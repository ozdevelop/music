import sys, os, subprocess as sp

try:
    id = sys.argv[1]
except IndexError:
    print("Missing video ID/URL!")
    exit(1)

if any([id.lower().startswith(c) for c in ["http","www","youtube"]]):
    id = id.split("/")[-1]

if len(id) != 11:
    print("Invalid id! <"+str(id)+">")
    exit(2)

try:
    pl = sys.argv[2]
except IndexError:
    print("Missing playlist directory/file!")
    exit(3)

if '.' in pl:
    pl = '.'.join(pl.split('.')[:-1])
if pl[-1] == '/':
    pl = pl[:-1]

print(f"Playlist Name: {pl}")

os.chdir(pl)
print(os.getcwd())
r='-'.join(sp.Popen(["fish", "-c",
    f"youtube-dl -x --audio-format mp3 https://youtu.be/{id} > /dev/null && ls | grep '{id}'"]
    ,stdout=sp.PIPE).communicate()[0].decode().split('-')[:-1])
os.chdir('..')
print(os.getcwd())
print("Downloaded Song -",r)

f = open(pl+'.ytl')
s = f.read()
f.close()
if id in s:
    print("Song already in playlist.")
else:
    s += id
    print(s)
    f = open(pl+'.ytl','w+')
    f.write(s)
    f.close()
    print("Wrote song to playlist.")

