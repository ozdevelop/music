#!/usr/bin/python3.8

from os import listdir, path, mkdir, rename, system

from sys import argv

got_args = False

output_folder = ""

playlist = ""

file_type = ".ytl"
song_file_type = ".mp3"

yt_prefix = "https://www.youtube.com/watch?v="

download_format = "mp3"

no_down_rem = False

out_file=None

no_whitelist=False

download_dir=""

down_opt = None
del_opt = None

#print(argv)

## parse commend line arguments
if any(
    [argv[i].startswith("-") for i in range(1, len(argv))]
):  # if any of the arguments start with '-'
    out = 0
    pllst = 0
    fil = 0
    down = 0
    dopt = 0
    delopt = 0
    for i in range(1, len(argv)):  # run on argumetns
        #print("arg:", argv[i])
        if out == 1:  # if out flag is on
            output_folder = argv[i]  # save output folder
            out = 2  # mark as used
            continue  # skip loop

        if pllst == 1:  # if playlist flag is on
            playlist = argv[i]  # save playlist file name
            pllst = 2  # mark as used
            continue  # skip loop

        if fil == 1:
            out_file = argv[i]
            fil = 2
            continue

        if down == 1:
            download_dir = argv[i]
            down = 2
            continue

        if dopt == 1:
            down_opt = argv[i]
            dopt = 2
            continue

        if delopt == 1:
            del_opt = argv[i]
            delopt = 2
            continue

        t = argv[i].lower()  # get argument at index to lowercase
        r = None
        if t.startswith("--"):  # if has 2 dashes
            t = t[2:]  # remove them
        else:  # one dash
            t = t[1:]  # remove it
            if len(t) != 1:
                r=[c for c in t]
        if r == None:
            r = [t]

        for t in r:
            # switch on argument string
            if t == "help" or t == "h":  # print help message
                print(
                    """
    Automatically downloads a .ytl playlist file.
    Opetions are:
        --help -h                           - prints this message.
        --output=file -o file               - sets the output folder path.
        --playlist=filename -p filename     - sets the playlist file name to download (only if passing other arguments). 
        --no-download-remove -n             - prevents actuall download and/or removal of songs.
        """
                )
                exit(0)
            elif t.startswith("output") or t == "o":
                if out != 0:
                    print("Too many of the same argument type! Exiting.")
                    exit(3)

                if "=" in t:
                    output_folder = t.split("=")[-1]
                    out = 2
                else:
                    output_folder = argv[i + 1]
                    out = 1
            elif t.startswith("playlist") or t == "p":
                if pllst != 0:
                    print("Too many of the same argument type! Exiting.")
                    exit(3)

                if "=" in t:
                    playlist = t.split("=")[-1]
                    pllst = 2
                else:
                    playlist = argv[i + 1]
                    pllst = 1
            elif t == "no-download-remove" or t == 'n':
                no_down_rem = True
            elif t.startswith('out-file') or t == 'f':
                if fil != 0:
                    print("Too many output files! Exiting.")
                    exit(4)

                if '=' in t:
                    out_file = t.split('=')[-1]
                    fil = 2
                else:
                    out_file = argv[i + 1]
                    fil = 1
            elif t == 'w':
                no_whitelist=True
            elif t.startswith('download-dir') or t == 'd':
                if down != 0:
                    print("Too many download directories! Exiting.")
                    exit(5)

                if '=' in t:
                    download_dir = t.split('=')[-1]
                    down = 2
                else:
                    download_dir = argv[i + 1]
                    down = 1
            elif t.startswith('download-option') or t == 'do':
                if dopt != 0:
                    print("Too many download options! Exitiomg...")
                    exit(6)

                if '=' in t:
                    down_opt = t.split('=')[-1]
                    dopt = 2
                else:
                    down_opt = argv[i + 1]
                    dopt = 1
            elif t.startswith('delete-option') or t == 'delo':
                if delopt != 0:
                    print("Too many delete options! Exitiomg...")
                    exit(6)

                if '=' in t:
                    del_opt = t.split('=')[-1]
                    delopt = 2
                else:
                    del_opt = argv[i + 1]
                    delopt = 1


print(down_opt,del_opt)

##

if not download_dir.endswith('/'):
    download_dir += '/'

# if playlist wasn't initialized
if playlist == "" and len(argv) == 2:
    playlist = argv[-1]
elif playlist == "":
    print("Missing playlist path! <use -p/--playlist>")  # missing playlist
    exit(1)  # exit
#print(playlist)

# search for substring in entire string list
def s_in_s_list(s, l):
    for i,v in enumerate(l):  # run on list
        if s in v:  # if substring
            return i  # found
    return -1  # not found


# returns the song name by its hash and the playlist (if parse, parses the special chars); test:input(get_song_name_by_hash("6wDgMzYkrEU","ATC"))
def get_song_name_by_hash(hsh, playlist, parse=False):
    if not playlist.endswith(file_type):  # if given playlist isnt a file name
        playlist += file_type  # add file type
    if not path.isfile(playlist):  # if playlist file isnt found
        return None  # return failed to find
    r = None  # init return var as not found
    with open(playlist, "r") as f:  # open playlist file
        s = f.read()  # read file
        if hsh in s:  # if hash in file
            r = s.split(hsh + "\n")[-1].split("\n")[
                0
            ]  # split on hash\n, split the last part (pass the hash) by \n and get the first part
    return r if not parse else parse_song_name(r)  # return song name


# something_hello-12345678912.mp3
def get_hash(file):
    # print(file + ":", file[-15:-4])
    return file[-15:-4]  # get the hash of the file


# returns stripped file name (without its type)
def rm_type(f):
    return ".".join(f.split(".")[:-1])


# parses a song name to remove special chars; test:# parse_song_name("Against+The+Current%3A+Runaway+%28LYRIC+VIDEO%29")
def parse_song_name(name):
    n = name.split("%")  # split on special chars
    pairs = [
        ("%" + n[i + 1][:2], chr(int(n[i + 1][:2], 16))) for i in range(len(n) - 1)
    ]  # create a pair list of special char hash and the special char

    for pair in pairs:  # run on pair list
        name = name.replace(pair[0], pair[1])  # replace each pair

    name = name.replace("+", " ")  # replace + with whitespace

    return name  # return formatted name


downloads = []

removes = []

playlist_file = ""  # init filestr
if playlist.endswith(file_type):  # if as file
    playlist_file = playlist  # save fileStr
    playlist = rm_type(playlist)  # strip type and save at var
else:  # if as playlist name
    playlist_file = playlist + file_type  # save fileStr as name.mp3

#print(playlist,playlist_file, path.isfile(playlist_file), whitelist)

if path.isfile(playlist_file):  # if playlist is in WhiteList and the file exists
    with open(playlist_file, "r") as file:  # open the file

        s = file.read()  # read
        #print(s,playlist_file)
        s = s.split("\n")  # split on song seperator

        vids = s #[i[:11] for i in s]  # get hashes
        if "" in vids:  # if any empty strings
            vids.remove("")  # remove

        if not path.isdir(download_dir + playlist + "/"):  # if playlist directory doesnt exist
            print(
                "Playlist Directory Not Found!\nTrying to create playlist directory.."
            )  # print
            try:
                mkdir(download_dir + playlist + "/")  # try to create
            except OSError:  # failed to create dir
                print(
                    "filed to create directory",
                    download_dir + playlist + "/",
                    "make sure folder has write privilages!",
                )  # inform user
                exit(1)  # exit with error code 1
            print("Created Playlist Directory.")  # success
        else:  # directory found
            print("Playlist Directory Found.")  # print

        files = listdir(download_dir + playlist + "/")  # get all files in the dir

        # input(files)
        # get vid hashes not in files list
        for hsh in vids:  # run on hashes
            if s_in_s_list(hsh, files) == -1:  # if hash not in file list
                downloads.append(
                    (get_song_name_by_hash(hsh, playlist, True), hsh)
                )  # add (song_name, hash) to download list

        # get files not in vids list
        for sng in files:  # run on files in folder
            if (
                s_in_s_list(get_hash(sng), vids) == -1 and sng != ".dnld.sh"
            ):  # if file's hash isnt in hashes list and not .dnld.sh
                removes.append(sng)  # add filename to remove list

# check for songs without hash in filename
for pair in downloads:  # run on download pairs
    index = -1
    if (index := s_in_s_list(pair[0], removes)) != -1 and rm_type(removes[index]) == pair[0]:  # if song name in removes list
        print(pair[0])
        print(removes[index])
        print(rm_type(removes[index]) == pair[0])
        r = input(
            "Found file: "
            + pair[0]
            + "\n    in the removal list, and it appears in the download list as well.\n    Do you want to add the hash to the file name? [Y/n]: "
        )  # inform user and wait for input (N/Y)
        if r.lower() != "n":  # if user didnt input No (the default is Yes)
            rename(  # rename file
                # From:
                download_dir + playlist  #
                + "/"  # playlist folder/
                + pair[0]  # song name
                + song_file_type,  # file type (.mp3)
                # To:
                download_dir + playlist  # playlist folder
                + "/"  # /
                + pair[0]  # song name
                + "-"  # seperator
                + pair[1]  # hash code
                + song_file_type,  # file type (.mp3)
            )
            downloads.remove(pair)  # remove pair from downloads
            removes.remove(pair[0] + song_file_type)  # remove file from removes

al = False
rem=[]
for i,p in enumerate(downloads):
    print("Song name:",p[0])
    if down_opt == None:
        r = input("Do you want to download it? [Y=yes/n=no/a=Yes to all]: ").lower()
    else:
        r = down_opt
    if r == 'n':
        rem.append(p)
    elif r == 'a':
        break
for r in rem:
    downloads.remove(r)

rem = []
for i,s in enumerate(removes):
    print("Song file name:",rm_type(s))
    if del_opt == None:
        r = input("Do you want to delete the song file? [y/N]: ").lower()
    else:
        r = del_opt
    if r != 'y':
        rem.append(s)
for i in rem:
    removes.remove(i)
print()
if len(downloads) != 0:  # if any downloads
    print("Songs To Download:")  # print to user
    print("    -", "\n    - ".join([pair[0] for pair in downloads]), "\n")
else:  # no downloads
    print("No Songs To Download.\n")  # inform

if len(removes) != 0:  # if any
    print("Song Files To Remove: ")  # print to user
    print("    -", "\n    - ".join(removes), "\n")
else:  # no removes
    print("No Songs To Remove.\n")  # inform

if out_file != None and not path.exists(out_file): 
    output = "#!/usr/bin/fish\n\n"
else:
    output = ""

playlist = download_dir + playlist

output += "cd " + playlist + "/\n\n"

l = "youtube-dl -x --audio-format "+download_format+" "+' '.join(['"'+yt_prefix+p[1]+'"' for p in downloads])
#print(l)
#print("\n\nyoutube-dl -x --audio-format mp3 "+l)
if len(downloads) != 0:
    print("Downloading Mew Songs... (command:"+l+")")
    if no_down_rem:
        print("No Download flag passed. skipping download.")
    else:
        if out_file != None:
            print("output file given, saving commands to file.")
            output += l + '\n\n'
        else:
            system("cd "+playlist+"/;"+l)

if len(removes) != 0:
    print("\n\nRemoving Old Songs... (command:cd "+playlist+"/; rm \""+"\" \"".join(removes)+"\")")
    if no_down_rem:
        print("No Remove flag passed. Skipping file removal.")
    else:
        if out_file != None:
            print("Output file given, saving commands to file.")
            output += 'rm "' + '" "'.join(removes) + '"'
        else:
            system("cd "+playlist+"/;rm \""+"\" \"".join(removes) + "\"")

output += "\n\ncd ../\n\n"
if out_file != None and not no_down_rem and len(downloads) != 0 or len(removes) != 0 and output != None and False:
    f = open(out_file, "a+")
    f.write(output)
    f.close()

